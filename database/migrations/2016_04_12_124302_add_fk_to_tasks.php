<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `tasks` MODIFY `session_id` INTEGER UNSIGNED NOT NULL;');
            $table->foreign('session_id')->references('id')->on('sessions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `tasks` MODIFY `session_id` INTEGER UNSIGNED NOT NULL;');
            $table->dropForeign(['session_id']);
        });
    }
}
