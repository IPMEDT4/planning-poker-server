<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->post('/session', 'SessionController@create');
$app->get('/session/{session_code}/users', 'SessionController@getUsersBySessionCode');
$app->post('/session/{session_code}','SessionController@joinSession');
$app->get('/session/{session_code}', 'SessionController@show');
$app->delete('/session/{session_id}', 'SessionController@delete');
$app->post("/session/end/{session_code}",'SessionController@endSession');
$app->post("/session/kick/{session_id}/{kick_user_id}",'SessionController@kickUser');

$app->post('/user', 'UserController@create');
$app->patch('/user', 'UserController@update');
$app->get('/user/session/{user_id}', 'UserController@getSessionByUser');

$app->get('/task/sessionid/{session_id}', 'TaskController@getTasksBySessionId');
$app->get('/task/{session_id}/{user_id}', 'TaskController@getBySession');
$app->post('/task', 'TaskController@create');
$app->patch('/task', 'TaskController@update');
$app->delete('/task', 'TaskController@delete');

$app->post('/task/next','TaskController@nextTask');
$app->post('/task/card','TaskController@setUserTaskValue');
$app->post('/task/reset','TaskController@resetTask');