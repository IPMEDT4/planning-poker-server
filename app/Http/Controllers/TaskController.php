<?php

namespace App\Http\Controllers;

use App\SessionModel;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    /**
     * @api {post} /task/ Create task for session
     * @apiName  Create
     * @apiGroup Task
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_code Session unique ID.
     * @apiParam {String} description Task description.
     * @apiParam {String} value Task value (not required).
     *
     * @apiSuccess {Object} task task object data.
     *
     * @apiErrorExample {JSON} Error-example
     *  422 Unprocessable Entity
     *  {
     *      "user_id": [
     *          "The user id field is required."
     *      ],
     *      "session_code": [
     *          "The session code field is required."
     *      ]
     *  }
     * @apiErrorExample {JSON} Error-example2
     *  422 Unprocessable Entity
     *  {
     *      "description": [
     *          "The description field is required."
     *      ]
     * }
     * @apiSuccessExample {JSON} Success-example
     *  200 OK
     *  {
     *      "description": "doe de afwas",
     *      "updated_at": "2016-03-15 12:37:16",
     *      "created_at": "2016-03-15 12:37:16",
     *      "id": 1
     *  }
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'session_code' => 'required|exists:sessions,session_code',
            'description' => 'required',
            'value' => 'String',

        ]);

        $input = $request->all();
        $session = SessionModel::findBySessionCode($request->get('session_code'));

        $input['session_id'] = $session->id;
        $task = Task::create($input);

        //push to clients
        $this->pushToClient($request->get('session_code'));
        return $task;


    }

    /**
     * @api {get} /task/:session_code/:user_id get tasks for session
     * @apiName  Get
     * @apiGroup Task
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} session_code Session unique ID.
     * @apiParam {Number} user_id Users unique ID.
     *
     * @apiSuccess {Object} task task object data.
     */
    public function getBySession(Request $request, $user_id, $session_code)
    {
        $session = SessionModel::findBySessionCode($session_code);

        if (User::CheckifInSessionCode($user_id, $session->id)) {

            return Task::where('session_id', $session->id)->get();
        }
        return abort(403, "No access");
    }

    /**
     * @api {patch} /task/ Update task for session
     * @apiName  Update
     * @apiGroup Task
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_code Session unique ID.
     *
     * @apiSuccess {Object} task task object data.
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'session_code' => 'required|exists:sessions,session_code',
            'task_id' => 'required|exists:tasks,id',
            'description' => 'required',
            'value' => 'String',

        ]);

        $session = SessionModel::findBySessionCode($request->get('session_code'));

        if (User::CheckifInSessionCode($request->get('user_id'), $session->id)) {
            $input = $request->all();
            Task::find($request->get('task_id'))->update($input);
            $this->pushToClient($request->get('session_code'));
            return Task::find($request->get('task_id'));
        }
        return abort(403, "No access");
    }

    /**
     * @api {delete} /task Delete task for session
     * @apiName  Delete
     * @apiGroup Task
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_code Session unique ID.
     * @apiParam {Number} task_id Task unique ID.
     *
     * @apiSuccess {String} result result.
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'session_code' => 'required|exists:sessions,session_code',
            'task_id' => 'required|exists:tasks,id',

        ]);

        $session = SessionModel::findBySessionCode($request->get('session_code'));

        if (User::CheckifInSessionCode($request->get('user_id'), $session->id)) {
            Task::find($request->get('task_id'))->delete();
            $request->get($request->get('session_code'));
            return "OK";
        }
    }


    /**
     * @api {post} /task/next Next task in session
     * @apiName  next task
     * @apiGroup Task
     * @apiVersion 0.1.2
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_code Session unique ID.
     *
     * @apiSuccess {String} result result.
     */
    public function nextTask(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'session_code' => 'required|exists:sessions,session_code',

        ]);

        $session = SessionModel::findBySessionCode($request->get('session_code'));

        if (User::CheckifInSessionCode($request->get('user_id'), $session->id)) {

            $tasks = Task::getBySessionId($session->id);

            $task = Task::where('session_id', $session->id)->where('id', '>', $session->current_task)->first();

            if (!is_null($task)) {
                $session->current_task = $task->id;
                $session->show_task_result = 0;
                $session->save();

                //reset all card input
                $this->resetUserCards($request->get('session_code'));

                //push message to clients
                $this->pushToClient($request->get('session_code'));

                return ["status" => "Next task asignt"];
            } else {
                return ["status" => "No more tasks"];
            }
        }

        abort(404, "WTF you trying to do!");
    }

    /**
     * @api {post} /task/card set card for current task
     * @apiName  card for current task
     * @apiGroup Task
     * @apiVersion 0.1.2
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {String} card value (0,0.5,1,2,3,5,8,13,20,40,100,COFFEE,INFINITY,QUESTIONMARK).
     * @apiParam {Number} session_code Session unique ID.
     *
     * @apiSuccess {String} result result.
     */
    public function setUserTaskValue(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'session_code' => 'required|exists:sessions,session_code',
            'card' => 'required|in:0,0.5,1,2,3,5,8,13,20,40,100,COFFEE,INFINITY,QUESTIONMARK',
        ]);

        $session = SessionModel::findBySessionCode($request->get('session_code'));

        if (User::CheckifInSessionCode($request->get('user_id'), $session->id)) {

            //update card in user profile
            $user = User::find($request->get('user_id'));
            $user->card = $request->get('card');
            $user->save();

            //check if all cards are set in session
            $this->isAllCardsSet($request->get('session_code'));
            return ["status" => "OK"];
        }
        abort(403, "WTF, what you trying to do!");

    }

    private function isAllCardsSet($session_code)
    {
        $session = SessionModel::findBySessionCode($session_code);

        foreach ($session->user as $user) {
            if (is_null($user->card)) {
                //card from a user is not set
                $session->show_task_result = 0;
                $session->save();
                return false;
            }
        }
        //all cards are set
        $session->show_task_result = 1;
        $session->save();

        $task = Task::find($session->current_task);
        $task->value = $session->user[0]->card;
        $task->save();

        $this->pushToClient($session_code);
    }

    private function resetUserCards($session_code)
    {
        $session = SessionModel::findBySessionCode($session_code);

        foreach ($session->user as $user) {
            $user->card = null;
            $user->save();
        }
    }

    /**
     * @api {post} /task/reset reset task input
     * @apiName  reset task input
     * @apiGroup Task
     * @apiVersion 0.1.2
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_code Session unique ID.
     *
     * @apiSuccess {String} result result.
     */
    public function resetTask(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'session_code' => 'required|exists:sessions,session_code',

        ]);

        $session = SessionModel::findBySessionCode($request->get('session_code'));

        if (User::CheckifInSessionCode($request->get('user_id'), $session->id)) {

            $this->resetUserCards($request->get('session_code'));
            $this->pushToClient($request->get('session_code'));
            return ["status" => "OK"];
        }
        abort(403, "WTF, what you trying to do!");
    }

    /**
     * @api {get} /task/sessionid/:session_id get task by session ID
     * @apiName  get task for closed session
     * @apiGroup Task
     * @apiVersion 0.1.4
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_id Session unique ID.
     *
     * @apiSuccess {String} result result.
     */
    public function getTasksBySessionId(Request $request, $session_id) {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
        ]);

        return Task::getBySessionId($session_id);
    }
}
