<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @api {post} /user Create user
     * @apiName  Create
     * @apiGroup User
     * @apiVersion 0.1.0
     *
     * @apiParam {String} name Users name.
     * @apiParam {String} gcm-token Users unique Google cloud messaging serverice token.
     * @apiParam {String} email Users email.
     *
     * @apiSuccess {Object} user  user object data.
     */
    public function create(Request $request) {

        $user = User::where('gcm-token',$request->get('gcm-token'))->first();

        $this->validate($request, [
            'name' => 'required',
            'gcm-token' => 'required',
            'email' => 'email',
        ]);

        if(!is_null($user)) {
            $user->name = $request->get('name');
            $user->save();
            return $user;
        }
        return User::create($request->only(['name','gcm-token']));
    }

    /**
     * @api {get} /user/:user_id get session by user
     * @apiName  get session by user
     * @apiGroup User
     * @apiVersion 0.1.0
     *
     * @apiParam {String} user_id User unique ID.
     *
     * @apiSuccess {Object} user  user en session object data.
     */
    public function getSessionByUser($user_id) {
        return User::where('id',$user_id)->with('session')->first();
    }

    /**
     * @api {patch} /user/:user_id Update user
     * @apiName  Update
     * @apiGroup User
     * @apiVersion 0.1.0
     *
     * @apiParam {String} user_id User unique ID.
     * @apiParam {String} name User name.
     *
     * @apiSuccess {Object} user  user object data.
     */
    public function update(Request $request) {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'name' => 'required',
        ]);
        User::find($request->get('user_id'))->update(['name' => $request->get('name')]);

        return User::find($request->get('user_id'));
    }

}
