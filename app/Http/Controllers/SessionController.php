<?php

namespace App\Http\Controllers;

use App\Events\GCMPusherEvent;
use App\SessionModel;
use Illuminate\Http\Request;
use App\User;
use Event;

class SessionController extends Controller
{

    /**
     * @api {post} /session/ Create session
     * @apiName  Create
     * @apiGroup Session
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} user_id Users unique ID.
     *
     * @apiSuccess {Object} session  session object data.
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "session_code":61,
     *          "updated_at":"2016-03-15 12:14:34",
     *          "created_at":"2016-03-15 12:14:34"
     *      }
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *          "user_id":["The user id field is required."]
     *     }
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
        ]);

        $session_code = random_int(0, 999);
        while (SessionModel::existSessionCode($session_code) == true) {
            $session_code = random_int(0, 999);
        }

        $session = SessionModel::create(['session_code' => $session_code]);
        $this->joinSession($request, $session_code);

        return $session;
    }

    /**
     * @api {get} /session/:session_code Get session object
     * @apiName  show
     * @apiGroup Session
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_id Session unique code.
     *
     * @apiSuccess {Object} session  session object data.
     */
    public function show(Request $request, $session_code)
    {
        $session = SessionModel::findBySessionCode($session_code);

            if (User::CheckifInSessionCode($request->get('user_id'), $session->id)) {
                $session->load('current_task');

                return $session;
            }

        abort(404, "no for you");
    }

    /**
     * @api {post} /session/:session_code Join session
     * @apiName  Join
     * @apiGroup Session
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} user_id Users unique ID.
     * @apiParam {Number} session_id Session unique code.
     *
     * @apiSuccess {Object} session  session object data.
     *
     * @apiError  message session not found
     * @apiError  status error
     */
    public function joinSession(Request $request, $session_code)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',

        ]);
        $session = null;

        //cleanup user card value
        $user = User::find($request->get('user_id'));
        $user->card = null;
        $user->save();

        if (SessionModel::addUserToSession($session_code, $request->get('user_id'))) {
            $this->pushToClient($session_code);
            $session = SessionModel::findBySessionCode($session_code);
        }

        if (is_null($session)) {
            return response()->json(['status' => 'error', 'message' => 'session not found'], 404);
        }

        $session->load('current_task');
        return $session;
    }

    /**
     * @api {delete} /session/:session_id Delete session
     * @apiName  Delete
     * @apiGroup Session
     * @apiVersion 0.1.0
     *
     * @apiParam {Number} session_id Session unique code.
     *
     * @apiSuccess {Object} session  session object data.
     */
    public function delete($session_code)
    {
        SessionModel::findBySessionCode($session_code)->delete();
    }

    /**
     * @api {get} /session/:session_code/users get users by session code
     * @apiName  get users
     * @apiGroup Session
     * @apiVersion 0.1.1
     *
     * @apiParam {Number} session_code Session unique code.
     *
     * @apiSuccess {Object} users  users object data.
     *
     */
    public function getUsersBySessionCode(Request $request, $session_code)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
        ]);

        return SessionModel::findBySessionCode($session_code)->user;
    }


    /**
     * @api {post} /session/end/:session_code end session
     * @apiName  end session
     * @apiGroup Session
     * @apiVersion 0.1.3
     *
     * @apiParam {Number} session_code Session unique code.
     * @apiParam {Number} user_id user unique ID.
     *
     * @apiSuccess {Object} status  status object data.
     */
    public function endSession(Request $request, $session_code)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
        ]);

        $session = SessionModel::findBySessionCode($session_code);
        $session->ended = 1;
        $session->save();

        $this->pushToClient($session_code);
        return ['status' => 'session ended'];
    }

    /**
     * @api {post} /session/kick/:session_id/:kick_user_id kick user
     * @apiName  kick user
     * @apiGroup Session
     * @apiVersion 0.1.4
     *
     * @apiParam {Number} session_id Session unique ID (not session code).
     * @apiParam {Number} kick_user_id user unique ID.
     * @apiParam {Number} user_id user unique ID.
     *
     * @apiSuccess {Object} status  status object data.
     *
     */
    public function kickUser(Request $request, $session_code, $kick_user_id)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
        ]);

        $session = SessionModel::findBySessionCode($session_code);
    }

}
