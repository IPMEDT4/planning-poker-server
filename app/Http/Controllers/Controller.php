<?php

namespace App\Http\Controllers;

use App\SessionModel;
use Laravel\Lumen\Routing\Controller as BaseController;
use Event;
use App\Events\GCMPusherEvent;

class Controller extends BaseController
{

    public function pushToClient($session_code)
    {

        //load data from session
        $session = SessionModel::findBySessionCode($session_code);
        if(!is_null($session)) {
            $session->load('task', 'user','current_task');

            $arrayData = [
                'message' => 'session data',
                'session' =>$session->toArray()
            ];

//        echo "<pre>";
//        print_r($arrayData);
//        echo "</pre>";

            Event::fire(new GCMPusherEvent($arrayData, $session->user));

        }
    }
}
