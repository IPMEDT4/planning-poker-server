<?php

namespace App\Events;

class GCMPusherEvent extends Event
{

    /**
     * GCMPusherEvent constructor.
     * @param array $data
     * @param $users
     */
    public function __construct(Array $data, $users)
    {

        $registerTokens = [
            //"cXTv_d6L9eg:APA91bGkJNwDvYs2n7yDdg7wSWNs2OFwl-dgr8G7QaURMf3JWM5HgcIPU6AWUKfEfgEiEJp8K0zQUmvQODDsb2mllfzKO00lpHholb7SQScpKKxGmODovKMnZglpEvoKXrru9kAHlxf5"
        ];

        foreach ($users as $user) {
            $registerTokens[] = $user['gcm-token'];
        }

        $data = [
            'data' => $data,
            'registration_ids' => $registerTokens
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://android.googleapis.com/gcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "authorization: key=" . env('GOOGLE_CLOUD_MESSAGING_SERVER_KEY', ""),
                "cache-control: no-cache",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        //debug stuff
//        if ($err) {
//            echo "cURL Error #:" . $err;
//        } else {
//            echo $response;
//        }
    }

}
