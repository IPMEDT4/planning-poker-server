<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionModel extends Model
{

    protected $table = "sessions";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['session_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    public static function existSessionCode($code)
    {
        return SessionModel::where('session_code', $code)->where('ended',0)->count() == 1;
    }

    public static function findBySessionCode($code)
    {
        return SessionModel::where('session_code', $code)->where('ended',0)->first();
    }

    public function user()
    {
        return $this->belongsToMany('App\User', 'session_user', 'session_id', 'user_id')->withTimestamps();
    }

    public function task()
    {
        return $this->hasMany('App\Task', 'session_id');
    }

    public function current_task()
    {
        return $this->hasOne('App\Task', 'id', 'current_task');
    }

    public static function addUserToSession($session_code, $user_id)
    {
        $session = SessionModel::findBySessionCode($session_code);

        //if session exist or is not ended
        if ($session != null && $session->ended == 0) {
            //is user not is session add to session
            if (User::CheckifInSessionCode($user_id, $session->id) === false) {
                $session->user()->attach($user_id);
            }
            return true;
        } else
            return false;
    }

}
