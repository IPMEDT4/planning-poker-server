define({ "api": [
  {
    "type": "post",
    "url": "/session/",
    "title": "Create session",
    "name": "Create",
    "group": "Session",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "session",
            "description": "<p>session object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"session_code\":61,\n     \"updated_at\":\"2016-03-15 12:14:34\",\n     \"created_at\":\"2016-03-15 12:14:34\"\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"user_id\":[\"The user id field is required.\"]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "delete",
    "url": "/session/:session_id",
    "title": "Delete session",
    "name": "Delete",
    "group": "Session",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_id",
            "description": "<p>Session unique code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "session",
            "description": "<p>session object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "post",
    "url": "/session/:session_code",
    "title": "Join session",
    "name": "Join",
    "group": "Session",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_id",
            "description": "<p>Session unique code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "session",
            "description": "<p>session object data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "message",
            "description": "<p>session not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "status",
            "description": "<p>error</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "post",
    "url": "/session/end/:session_code",
    "title": "end session",
    "name": "end_session",
    "group": "Session",
    "version": "0.1.3",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique code.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>user unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "status",
            "description": "<p>status object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "get",
    "url": "/session/:session_code/users",
    "title": "get users by session code",
    "name": "get_users",
    "group": "Session",
    "version": "0.1.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "users",
            "description": "<p>users object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "post",
    "url": "/session/kick/:session_id/:kick_user_id",
    "title": "kick user",
    "name": "kick_user",
    "group": "Session",
    "version": "0.1.4",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_id",
            "description": "<p>Session unique ID (not session code).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "kick_user_id",
            "description": "<p>user unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>user unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "status",
            "description": "<p>status object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "get",
    "url": "/session/:session_code",
    "title": "Get session object",
    "name": "show",
    "group": "Session",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_id",
            "description": "<p>Session unique code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "session",
            "description": "<p>session object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/SessionController.php",
    "groupTitle": "Session"
  },
  {
    "type": "post",
    "url": "/task/",
    "title": "Create task for session",
    "name": "Create",
    "group": "Task",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Task description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>Task value (not required).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "task",
            "description": "<p>task object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-example",
          "content": "200 OK\n{\n    \"description\": \"doe de afwas\",\n    \"updated_at\": \"2016-03-15 12:37:16\",\n    \"created_at\": \"2016-03-15 12:37:16\",\n    \"id\": 1\n}",
          "type": "JSON"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-example",
          "content": "422 Unprocessable Entity\n{\n    \"user_id\": [\n        \"The user id field is required.\"\n    ],\n    \"session_code\": [\n        \"The session code field is required.\"\n    ]\n}",
          "type": "JSON"
        },
        {
          "title": "Error-example2",
          "content": " 422 Unprocessable Entity\n {\n     \"description\": [\n         \"The description field is required.\"\n     ]\n}",
          "type": "JSON"
        }
      ]
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "delete",
    "url": "/task",
    "title": "Delete task for session",
    "name": "Delete",
    "group": "Task",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "task_id",
            "description": "<p>Task unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>result.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/task/:session_code/:user_id",
    "title": "get tasks for session",
    "name": "Get",
    "group": "Task",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "task",
            "description": "<p>task object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "patch",
    "url": "/task/",
    "title": "Update task for session",
    "name": "Update",
    "group": "Task",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "task",
            "description": "<p>task object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/card",
    "title": "set card for current task",
    "name": "card_for_current_task",
    "group": "Task",
    "version": "0.1.2",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "card",
            "description": "<p>value (0,0.5,1,2,3,5,8,13,20,40,100,COFFEE,INFINITY,QUESTIONMARK).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>result.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/task/sessionid/:session_id",
    "title": "get task by session ID",
    "name": "get_task_for_closed_session",
    "group": "Task",
    "version": "0.1.4",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_id",
            "description": "<p>Session unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>result.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/next",
    "title": "Next task in session",
    "name": "next_task",
    "group": "Task",
    "version": "0.1.2",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>result.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/reset",
    "title": "reset task input",
    "name": "reset_task_input",
    "group": "Task",
    "version": "0.1.2",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "session_code",
            "description": "<p>Session unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>result.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/TaskController.php",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Create user",
    "name": "Create",
    "group": "User",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Users name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gcm-token",
            "description": "<p>Users unique Google cloud messaging serverice token.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>user object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "patch",
    "url": "/user/:user_id",
    "title": "Update user",
    "name": "Update",
    "group": "User",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>user object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/:user_id",
    "title": "get session by user",
    "name": "get_session_by_user",
    "group": "User",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>user en session object data.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User"
  }
] });
